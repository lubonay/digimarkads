package com.npace.digimarkads;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.npace.digimarkads.myads.MyAdsFragment;
import com.npace.digimarkads.placead.AdCreatedListener;
import com.npace.digimarkads.placead.PlaceAdFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements AdCreatedListener {

    private static final String KEY_PLACE_AD_STATE = "place_ad_state";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbarCustomTitle)
    TextView toolbarCustomTitle;
    @BindView(R.id.content)
    FrameLayout content;
    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    private MyAdsFragment myAdsFragment;
    private PlaceAdFragment placeAdFragment;
    private Fragment.SavedState placeAdState;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            int itemId = item.getItemId();
            if (itemId != navigation.getSelectedItemId()) {
                switch (itemId) {
                    case R.id.navigation_my_ads:
                        placeAdState = getSupportFragmentManager().saveFragmentInstanceState(getSupportFragmentManager().findFragmentByTag(PlaceAdFragment.TAG));
                        replaceContent(myAdsFragment, MyAdsFragment.TAG);
                        return true;
                    case R.id.navigation_place_ad:
                        placeAdFragment.setInitialSavedState(placeAdState);
                        replaceContent(placeAdFragment, PlaceAdFragment.TAG);
                        return true;
                }
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        myAdsFragment = (MyAdsFragment) getSupportFragmentManager().findFragmentByTag(MyAdsFragment.TAG);
        if (myAdsFragment == null) {
            myAdsFragment = new MyAdsFragment();
        }

        placeAdFragment = (PlaceAdFragment) getSupportFragmentManager().findFragmentByTag(PlaceAdFragment.TAG);
        if (placeAdFragment == null) {
            placeAdFragment = new PlaceAdFragment();
        }

        if (savedInstanceState == null) {
            replaceContent(myAdsFragment, MyAdsFragment.TAG);
        } else {
            placeAdState = savedInstanceState.getParcelable(KEY_PLACE_AD_STATE);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(KEY_PLACE_AD_STATE, placeAdState);
        super.onSaveInstanceState(outState);
    }

    private void replaceContent(Fragment fragment, String tag) {
        Fragment current = getSupportFragmentManager().findFragmentById(R.id.content);
        if (current != null) getSupportFragmentManager().saveFragmentInstanceState(current);
        getSupportFragmentManager().beginTransaction().replace(R.id.content, fragment, tag).commit();
    }

    @Override
    public void onAdCreated() {
        navigation.setSelectedItemId(R.id.navigation_my_ads);
        placeAdState = null;
        placeAdFragment = new PlaceAdFragment();
    }

    public void showTitle(String title) {
        toolbarCustomTitle.setText(title);
    }
}
