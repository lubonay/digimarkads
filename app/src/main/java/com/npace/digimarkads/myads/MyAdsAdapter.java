package com.npace.digimarkads.myads;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.npace.digimarkads.R;
import com.npace.digimarkads.serialization.AdJsonSerializer;
import com.npace.digimarkads.state.Ad;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lubo on 3/20/2017.
 */

public class MyAdsAdapter extends RecyclerView.Adapter<MyAdsAdapter.ViewHolder> {
    private static final int TYPE_AD = 0;
    private static final int TYPE_FOOTER = 1;

    private List<Ad> ads = Collections.emptyList();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        ViewHolder viewHolder;

        if (viewType == TYPE_AD) {
            view = inflater.inflate(R.layout.row_ad, parent, false);
            viewHolder = new AdViewHolder(view);
        } else {
            view = inflater.inflate(R.layout.row_footer, parent, false);
            viewHolder = new FooterViewHolder(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_AD) {
            bindAd((AdViewHolder) holder, ads.get(position));
        } else {
            bindFooter((FooterViewHolder) holder);
        }
    }


    private void bindFooter(FooterViewHolder holder) {
        holder.button.setOnClickListener(view -> logDataAsJson());
    }

    private void logDataAsJson() {
        try {
            AdJsonSerializer serializer = new AdJsonSerializer();
            JSONObject result = serializer.serialize(ads);
            Log.i("My Ads", result.toString(4));
        } catch (JSONException e) {
            Log.e("My Ads", "Could not serialize and print the ads!", e);
        }
    }

    private void bindAd(AdViewHolder holder, Ad ad) {
        Context context = holder.image.getContext();
        Picasso.with(context)
                .load(new File(ad.getMainThumbnail().path))
                .centerCrop()
                .fit()
                .into(holder.image);

        holder.title.setText(ad.getTitle());
        holder.price.setText(getPriceString(context, ad.getPrice()));
        holder.location.setText(ad.isShowLocation() ?
                context.getString(R.string.location_placeholder) :
                context.getString(R.string.not_specified));
        holder.description.setText(ad.getDescription());
    }

    private String getPriceString(Context context, float price) {
        if (price == 0) {
            return context.getString(R.string.not_specified);
        } else if (Math.floor(price) == price) {
            return context.getString(R.string.price_in_euro_integer, (int) price);
        } else {
            return context.getString(R.string.price_in_euro_decimal, price);
        }
    }

    @Override
    public int getItemCount() {
        return ads.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position == ads.size() ?
                TYPE_FOOTER :
                TYPE_AD;
    }

    public void setData(List<Ad> ads) {
        this.ads = ads;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    static class AdViewHolder extends ViewHolder {

        @BindView(R.id.image) ImageView image;
        @BindView(R.id.textViewTitle) TextView title;
        @BindView(R.id.textViewPrice) TextView price;
        @BindView(R.id.textViewLocation) TextView location;
        @BindView(R.id.textViewDescription) TextView description;


        public AdViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    static class FooterViewHolder extends ViewHolder {

        @BindView(R.id.button_save) Button button;

        public FooterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
