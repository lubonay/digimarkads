package com.npace.digimarkads.myads;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.npace.digimarkads.DigimarkApplication;
import com.npace.digimarkads.R;

/**
 * Created by lubo on 3/27/2017.
 */

public class CustomDividerItemDecoration extends RecyclerView.ItemDecoration {
    private final int horizontalMargin;
    private Drawable divider;

    public CustomDividerItemDecoration() {
        Context context = DigimarkApplication.get();
        this.divider = ContextCompat.getDrawable(context, R.drawable.list_divider);
        this.horizontalMargin = context.getResources().getDimensionPixelOffset(R.dimen.activity_horizontal_margin);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        int position = parent.getChildAdapterPosition(view);
        boolean isFirstChild = position == 0;
        boolean isLastChild = position == parent.getAdapter().getItemCount() - 1;
        if (!(isFirstChild || isLastChild)) {
            outRect.top = divider.getIntrinsicHeight();
        }
    }

    @Override
    public void onDraw(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
        int dividerLeft = parent.getPaddingLeft() + horizontalMargin;
        int dividerRight = parent.getWidth() - parent.getPaddingRight() - horizontalMargin;

        // -1 accounts for not putting a divider after the last child
        int count = parent.getChildCount() - 1;
        for (int i = 0; i < count; i++) {
            View child = parent.getChildAt(i);
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int dividerTop = child.getBottom() + params.bottomMargin;
            int dividerBottom = dividerTop + divider.getIntrinsicHeight();

            divider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom);
            divider.draw(canvas);
        }
    }
}
