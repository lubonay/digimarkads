package com.npace.digimarkads.myads;

import com.npace.digimarkads.state.Ad;

import java.util.List;

/**
 * Created by lubo on 3/20/2017.
 */

public interface MyAdsView {
    void showAdsList(List<Ad> ads);
}
