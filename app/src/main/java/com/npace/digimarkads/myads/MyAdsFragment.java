package com.npace.digimarkads.myads;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.npace.digimarkads.BaseFragment;
import com.npace.digimarkads.DigimarkApplication;
import com.npace.digimarkads.MainActivity;
import com.npace.digimarkads.R;
import com.npace.digimarkads.state.Ad;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import toothpick.Toothpick;

/**
 * Created by lubo on 3/20/2017.
 */

public class MyAdsFragment extends BaseFragment implements MyAdsView {
    public static final String TAG = MyAdsFragment.class.getSimpleName();

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Inject
    MyAdsPresenter presenter;

    private MyAdsAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_my_ads, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toothpick.inject(this, Toothpick.openScope(DigimarkApplication.get()));

        adapter = new MyAdsAdapter();
        recyclerView.addItemDecoration(new CustomDividerItemDecoration());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.bindView(this);
        ((MainActivity)getActivity()).showTitle(getString(R.string.title_my_ads));
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.unbindView();
    }

    @Override
    public void showAdsList(List<Ad> ads) {
        adapter.setData(ads);
    }
}
