package com.npace.digimarkads.myads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.npace.digimarkads.state.Ad;
import com.npace.digimarkads.state.AdStore;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by lubo on 3/20/2017.
 */
@Singleton
public class MyAdsPresenter {
    private @Nullable MyAdsView view;
    private AdStore adStore;

    @Inject
    public MyAdsPresenter(AdStore adStore) {
        this.adStore = adStore;
    }

    public void bindView(@NonNull MyAdsView view) {
        this.view = view;
        view.showAdsList(adStore.getAds());
    }

    public void unbindView() {
        this.view = null;
    }
}
