package com.npace.digimarkads;

import android.app.Application;

import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.smoothie.module.SmoothieApplicationModule;

/**
 * Created by lubo on 3/25/2017.
 */

public class DigimarkApplication extends Application {
    private static final String TAG = DigimarkApplication.class.getSimpleName();
    private static DigimarkApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        Scope scope = Toothpick.openScope(this);
        scope.installModules(new SmoothieApplicationModule(this));
    }

    public static DigimarkApplication get() {
        return instance;
    }

}
