package com.npace.digimarkads.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.npace.digimarkads.R;
import com.npace.digimarkads.state.Thumbnail;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lubo on 3/24/2017.
 */

public class SelectPictureView extends LinearLayout {
    @BindView(R.id.image)
    ImageView imageView;

    @BindView(R.id.text)
    TextView textView;

    public SelectPictureView(Context context) {
        this(context, null);
    }

    public SelectPictureView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SelectPictureView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.layout_select_picture_view, this, true);
        ButterKnife.bind(this);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SelectPictureView, defStyleAttr, 0);
        boolean isMain = a.getBoolean(R.styleable.SelectPictureView_isMain, false);
        setIsMain(isMain);
        a.recycle();
    }

    private void setIsMain(boolean isMain) {
        textView.setEnabled(isMain);
    }

    public void setThumbnail(Thumbnail thumbnail) {
        if (thumbnail == null || TextUtils.isEmpty(thumbnail.path)) {
            imageView.setImageResource(R.drawable.ic_photo);
            setIsMain(false);
        } else {
            setIsMain(thumbnail.isMain());
            Picasso.with(getContext())
                    .load(new File(thumbnail.path))
                    .fit()
                    .centerCrop()
                    .into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Toast.makeText(getContext(), R.string.failed_to_load_image, Toast.LENGTH_LONG).show();
                        }
                    });
        }
    }
}
