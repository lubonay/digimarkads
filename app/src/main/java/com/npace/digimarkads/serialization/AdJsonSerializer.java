package com.npace.digimarkads.serialization;

import com.npace.digimarkads.state.Thumbnail;
import com.npace.digimarkads.state.Ad;
import com.squareup.moshi.FromJson;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.ToJson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by lubo on 3/27/2017.
 */

public class AdJsonSerializer {
    private Moshi moshi;

    public AdJsonSerializer() {
        moshi = new Moshi.Builder()
                .add(new ThumbTypeAdapter())
                .build();
    }

    public JSONObject serialize(List<Ad> ads) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        JsonAdapter<Ad> adAdapter = moshi.adapter(Ad.class);
        for (Ad ad : ads) {
            String json = adAdapter.toJson(ad);
            jsonArray.put(new JSONObject(json));
        }

        JSONObject result = new JSONObject();
        result.put("ads", jsonArray);
        return result;
    }

    static class ThumbTypeAdapter {
        @FromJson
        Thumbnail.Type fromJson(String json) {
            throw new UnsupportedOperationException();
        }

        @ToJson
        String toJson(Thumbnail.Type type) {
            return type.toString().toLowerCase();
        }
    }

}
