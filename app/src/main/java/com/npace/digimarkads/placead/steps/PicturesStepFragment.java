package com.npace.digimarkads.placead.steps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.npace.digimarkads.R;
import com.npace.digimarkads.state.Thumbnail;
import com.npace.digimarkads.placead.thumb.ThumbnailRequestCodes;
import com.npace.digimarkads.placead.thumb.ThumbnailService;
import com.npace.digimarkads.widgets.SelectPictureView;

import java.util.List;

import butterknife.BindViews;

/**
 * Created by lubo on 3/24/2017.
 */

public class PicturesStepFragment extends StepFragment implements InputValidator, PlaceAdPresenter.ThumbnailListener {
    private static final String TAG = PicturesStepFragment.class.getSimpleName();

    @BindViews({R.id.add_photo_item_1, R.id.add_photo_item_2, R.id.add_photo_item_3, R.id.add_photo_item_4, R.id.add_photo_item_5, R.id.add_photo_item_6})
    List<SelectPictureView> selectPictureViews;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_add_photos, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        for (int i = 0; i < selectPictureViews.size(); i++) {
            final int index = i;
            SelectPictureView selectPictureView = selectPictureViews.get(i);
            selectPictureView.setOnClickListener(v -> {
                boolean hasThumb = index >= 0 && index < getPresenter().getThumbnails().size();
                if (hasThumb) {
                    PopupMenu popup = new PopupMenu(getContext(), v);
                    popup.inflate(R.menu.picture);
                    popup.setOnMenuItemClickListener(item -> {
                        if (item.getItemId() == R.id.picture_set_main) {
                            getPresenter().setMainThumbnail(index);
                        } else if (item.getItemId() == R.id.picture_remove) {
                            getPresenter().removeThumbnail(index);
                        }
                        return true;
                    });
                    popup.show();
                }
            });
        }

        updateThumbnails();
    }

    @Override
    public void onStart() {
        super.onStart();
        getPresenter().setThumbnailListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().setThumbnailListener(null);
        setHasOptionsMenu(false);
    }

    @Override
    public void onDataSetChanged() {
        updateThumbnails();
    }

    @Override
    public void onErrorGeneratingThumbnail(Exception e) {
        Toast.makeText(getContext(), R.string.error_generating_thumbnail, Toast.LENGTH_LONG).show();
    }

    private void updateThumbnails() {
        List<Thumbnail> thumbs = getPresenter().getThumbnails();
        for (int i = 0; i < thumbs.size(); i++) {
            Thumbnail thumb = thumbs.get(i);
            if (thumb != null) {
                selectPictureViews.get(i).setThumbnail(thumb);
            }
        }
        for (int i = thumbs.size(); i < selectPictureViews.size(); i++) {
            selectPictureViews.get(i).setThumbnail(null);
        }
        updateMenuVisibility();
    }

    private void updateMenuVisibility() {
        int size = getPresenter().getThumbnails().size();
        setHasOptionsMenu(size != getPresenter().getMaxPictureCount());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.add_picture, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.take_picture) {
            takePicture();
        } else if (item.getItemId() == R.id.browse_gallery) {
            browseGallery();
        }
        return true;
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            int requestCode = ThumbnailRequestCodes.encodeRequestCode(Thumbnail.Type.Camera);
            startActivityForResult(intent, requestCode);
        }
    }

    void browseGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            int requestCode = ThumbnailRequestCodes.encodeRequestCode(Thumbnail.Type.Gallery);
            startActivityForResult(intent, requestCode);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("npace", "onActivityResult() called with: requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data + "]");
        if (resultCode == Activity.RESULT_OK) {
            ThumbnailService.start(getContext(), data.getData(), requestCode);
        }
    }

    @Override
    public boolean validate() {
        List<Thumbnail> thumbnails = getPresenter().getThumbnails();
        boolean hasThumb = false;
        for (Thumbnail thumbnail : thumbnails) {
            if (thumbnail != null) {
                hasThumb = true;
                break;
            }
        }

        if (!hasThumb) {
            Toast.makeText(getContext(), R.string.at_least_one_picture_required, Toast.LENGTH_LONG).show();
        }

        return hasThumb;
    }
}
