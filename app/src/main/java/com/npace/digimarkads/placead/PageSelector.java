package com.npace.digimarkads.placead;

/**
 * Created by lubo on 3/26/2017.
 */

public class PageSelector {
    private int lastSelectedPage;
    private PageValidator pageValidator;

    public PageSelector(int lastSelectedPage, PageValidator pageValidator) {
        this.lastSelectedPage = lastSelectedPage;
        this.pageValidator = pageValidator;
    }

    public boolean canSelectPage(int page) {
        boolean isGoingForward = page > lastSelectedPage;
        if (isGoingForward) {
            int previousPosition = page - 1;
            if (!pageValidator.validate(previousPosition)) {
                return false;
            }
        }

        lastSelectedPage = page;
        return true;
    }

    int getLastSelectedPage() {
        return lastSelectedPage;
    }
}
