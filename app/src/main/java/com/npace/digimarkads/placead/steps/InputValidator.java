package com.npace.digimarkads.placead.steps;

/**
 * Created by lubo on 3/26/2017.
 */

public interface InputValidator {
    boolean validate();
}
