package com.npace.digimarkads.placead;

/**
 * Created by lubo on 3/26/2017.
 */

public interface PageValidator {
    boolean validate(int position);
}
