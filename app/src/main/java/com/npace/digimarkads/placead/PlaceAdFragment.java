package com.npace.digimarkads.placead;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.npace.digimarkads.BaseFragment;
import com.npace.digimarkads.DigimarkApplication;
import com.npace.digimarkads.MainActivity;
import com.npace.digimarkads.R;
import com.npace.digimarkads.placead.steps.AdInfoStepFragment;
import com.npace.digimarkads.placead.steps.CategoryStepFragment;
import com.npace.digimarkads.placead.steps.ContactsStepFragment;
import com.npace.digimarkads.placead.steps.InputValidator;
import com.npace.digimarkads.placead.steps.PicturesStepFragment;
import com.npace.digimarkads.placead.steps.PlaceAdPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import toothpick.Toothpick;

/**
 * Created by lubo on 3/20/2017.
 */

public class PlaceAdFragment extends BaseFragment {
    public static final String TAG = PlaceAdFragment.class.getSimpleName();
    private static final String KEY_LAST_POSITION = "last_position";

    @BindView(R.id.pager)
    ViewPager viewPager;

    @BindView(R.id.buttonNextStep)
    Button button;

    @Inject
    PlaceAdPresenter presenter;
    private PageSelector pageSelector;
    private PlaceAdPagerAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_place_ad, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toothpick.inject(this, Toothpick.openScope(DigimarkApplication.get()));

        int lastPosition = savedInstanceState != null ? savedInstanceState.getInt(KEY_LAST_POSITION, 0) : 0;
        adapter = new PlaceAdPagerAdapter(getChildFragmentManager());
        pageSelector = new PageSelector(lastPosition, adapter);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                showTitleForPosition(position);
                if (!pageSelector.canSelectPage(position)) {
                    int previousPosition = position - 1;
                    viewPager.setCurrentItem(previousPosition, true);
                }

                boolean isLastPage = position == adapter.getCount() - 1;
                button.setText(isLastPage ? R.string.save : R.string.next);
            }
        });
        button.setOnClickListener(view1 -> {
            int currentPage = viewPager.getCurrentItem();
            boolean isLastPage = currentPage == viewPager.getAdapter().getCount() - 1;
            if (isLastPage) {
                finishAdCreation();
            } else {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
            }
        });

        showTitleForPosition(viewPager.getCurrentItem());
    }

    private void showTitleForPosition(int position) {
        String title;
        if (position == 0) {
            title = getString(R.string.title_ad_info);
        } else if (position == 1) {
            title = getString(R.string.title_add_photos);
        } else if (position == 2) {
            title = getString(R.string.title_add_category);
        } else if (position == 3) {
            title = getString(R.string.title_add_contacts);
        } else {
            throw new IllegalStateException("No title defined for position " + position);
        }
        ((MainActivity)getActivity()).showTitle(title);
    }

    private void finishAdCreation() {
        adapter.validate(3);
        presenter.createAd();
        ((AdCreatedListener)getActivity()).onAdCreated();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(KEY_LAST_POSITION, viewPager.getCurrentItem());
        super.onSaveInstanceState(outState);
    }

    private class PlaceAdPagerAdapter extends FragmentPagerAdapter implements PageValidator {
        private static final int PAGES_COUNT = 4;

        private Fragment[] fragments;

        public PlaceAdPagerAdapter(FragmentManager fm) {
            super(fm);
            fragments = new Fragment[PAGES_COUNT];
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return new AdInfoStepFragment();
            } else if (position == 1){
                return new PicturesStepFragment();
            } else if (position == 2) {
                return new CategoryStepFragment();
            } else if (position == 3){
                return new ContactsStepFragment();
            } else {
                throw new IllegalStateException("No page fragment set for position " + position);
            }
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            fragments[position] = fragment;
            return fragment;
        }

        @Override
        public int getCount() {
            return PAGES_COUNT;
        }

        @Override
        public boolean validate(int position) {
            InputValidator validator = (InputValidator) fragments[position];
            return validator.validate();
        }
    }
}
