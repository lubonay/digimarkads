package com.npace.digimarkads.placead.steps;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.npace.digimarkads.BaseFragment;
import com.npace.digimarkads.DigimarkApplication;

import javax.inject.Inject;

import toothpick.Toothpick;

/**
 * Created by lubo on 3/26/2017.
 */

public class StepFragment extends BaseFragment {
    @Inject
    PlaceAdPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toothpick.inject(this, Toothpick.openScope(DigimarkApplication.get()));
    }

    protected PlaceAdPresenter getPresenter() {
        return presenter;
    }
}
