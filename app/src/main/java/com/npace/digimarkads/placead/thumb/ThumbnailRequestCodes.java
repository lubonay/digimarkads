package com.npace.digimarkads.placead.thumb;

import com.npace.digimarkads.state.Thumbnail;

/**
 * Created by lubo on 3/25/2017.
 */

public class ThumbnailRequestCodes {
    static final int ENCODED_CAMERA = 1;
    static final int ENCODED_GALLERY = 2;

    // don't instantiate
    private ThumbnailRequestCodes() {}

    public static int encodeRequestCode(Thumbnail.Type type) {
        return type == Thumbnail.Type.Camera ? ENCODED_CAMERA : ENCODED_GALLERY;
    }

    public static Thumbnail.Type decodeRequestCode(int requestCode) {
        return requestCode == ENCODED_CAMERA ? Thumbnail.Type.Camera : Thumbnail.Type.Gallery;
    }
}
