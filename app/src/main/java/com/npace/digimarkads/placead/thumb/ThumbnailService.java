package com.npace.digimarkads.placead.thumb;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.npace.digimarkads.DigimarkApplication;
import com.npace.digimarkads.placead.steps.PlaceAdPresenter;
import com.npace.digimarkads.state.Thumbnail;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Inject;

import toothpick.Toothpick;

public class ThumbnailService extends Service {
    private static final String TAG = ThumbnailService.class.getSimpleName();
    private static final String EXTRA_REQUEST_CODE = "extra_request_code";
    private ExecutorService executor;
    private Handler handler = new Handler(Looper.getMainLooper());

    @Inject PlaceAdPresenter placeAdPresenter;

    public static void start(Context context, Uri data, int requestCode) {
        Intent intent = new Intent(context, ThumbnailService.class);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setData(data);
        intent.putExtra(ThumbnailService.EXTRA_REQUEST_CODE, requestCode);
        context.startService(intent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toothpick.inject(this, Toothpick.openScope(DigimarkApplication.get()));
        executor = Executors.newSingleThreadExecutor();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand() called with: intent = [" + intent + "], flags = [" + flags + "], startId = [" + startId + "]");
        if (intent == null) return START_NOT_STICKY;

        final Uri data = intent.getData();
        final int requestCode = intent.getIntExtra(EXTRA_REQUEST_CODE, -1);

        executor.submit(() -> {
            if (requestCode == -1) {
                Log.e(TAG, "Got invalid index from start command intent!");
            } else if (data != null) {
                Thumbnail.Type type = ThumbnailRequestCodes.decodeRequestCode(requestCode);
                try {
                    File tempFile = saveToTempFile(data);
                    Thumbnail thumbnail = new Thumbnail(type, tempFile.getAbsolutePath());
                    runOnUiThread(() -> placeAdPresenter.addThumbnail(thumbnail));
                } catch (IOException e) {
                    e.printStackTrace();
                    runOnUiThread(() -> placeAdPresenter.onErrorGeneratingThumbnail(e));
                }

            }
        });

        return super.onStartCommand(intent, flags, startId);
    }

    private File saveToTempFile(Uri data) throws IOException {
        InputStream input = null;
        FileOutputStream output = null;
        String tempFileName = data.getLastPathSegment();
        File tempFile = null;
        try {
            input = getContentResolver().openInputStream(data);
            output = openFileOutput(tempFileName, 0);
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = input.read(buffer)) > 0) {
                output.write(buffer, 0, bytesRead);
                output.flush();
            }
            tempFile = new File(getFilesDir(), tempFileName);
        } finally {
            closeSilently(input);
            closeSilently(output);
        }
        return tempFile;
    }

    private void closeSilently(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException ignored) {
            }
        }
    }

    private void runOnUiThread(Runnable runnable) {
        handler.post(runnable);
    }
}
