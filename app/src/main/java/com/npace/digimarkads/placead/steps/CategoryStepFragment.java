package com.npace.digimarkads.placead.steps;

import android.os.Bundle;
import android.support.annotation.ArrayRes;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.npace.digimarkads.R;

import butterknife.BindView;

/**
 * Created by lubo on 3/24/2017.
 */

public class CategoryStepFragment extends StepFragment implements InputValidator {
    private static final String TAG = CategoryStepFragment.class.getSimpleName();

    @BindView(R.id.spinner_category)
    Spinner spinnerCategory;

    @BindView(R.id.spinner_subcategory)
    Spinner spinnerSubcategory;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_add_category, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(view.getContext(), R.array.categories, R.layout.spinner_item);
        spinnerCategory.setAdapter(adapter);
        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(TAG, "onItemSelected() called with: i = [" + i + "], l = [" + l + "]");
                spinnerSubcategory.setAdapter(ArrayAdapter.createFromResource(getContext(), getSubcategory(i), R.layout.spinner_item));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.d(TAG, "onNothingSelected() called with: adapterView = [" + adapterView + "]");
            }
        });
        spinnerSubcategory.setAdapter(ArrayAdapter.createFromResource(getContext(), getSubcategory(0), R.layout.spinner_item));
    }

    private @ArrayRes int getSubcategory(int index) {
        if (index == 0) {
            return R.array.subcategory_for_sale;
        } else if (index == 1) {
            return R.array.subcategory_services;
        } else if (index == 2) {
            return R.array.subcategory_vehicles;
        } else if (index == 3){
            return R.array.subcategory_property;
        } else {
            throw new IllegalArgumentException("Invalid subcategory index: " + index);
        }
    }

    @Override
    public boolean validate() {
        if (spinnerCategory.getSelectedItem() == null || spinnerSubcategory.getSelectedItem() == null) {
            Toast.makeText(getContext(), R.string.category_required, Toast.LENGTH_LONG).show();
            return false;
        } else {
            String category = spinnerCategory.getSelectedItem().toString();
            String subcategory = spinnerSubcategory.getSelectedItem().toString();
            getPresenter().setAdCategory(category, subcategory);
            return true;
        }
    }
}
