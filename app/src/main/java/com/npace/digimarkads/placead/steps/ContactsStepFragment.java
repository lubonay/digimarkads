package com.npace.digimarkads.placead.steps;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;

import com.npace.digimarkads.R;

import butterknife.BindView;

/**
 * Created by lubo on 3/24/2017.
 */

public class ContactsStepFragment extends StepFragment implements InputValidator {

    @BindView(R.id.editTextPhoneNumber)
    EditText editTextPhoneNumber;

    @BindView(R.id.checkboxShareOnFacebook)
    CheckBox checkBoxShareOnFacebook;

    @BindView(R.id.checkboxShowMyLocation)
    CheckBox checkBoxShowMyLocation;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_add_contacts, container, false);
    }

    @Override
    public boolean validate() {
        String phoneNumber = editTextPhoneNumber.getText().toString();
        boolean shareOnFacebook = checkBoxShareOnFacebook.isChecked();
        boolean showLocation = checkBoxShowMyLocation.isChecked();

        getPresenter().setContactInfo(phoneNumber, shareOnFacebook, showLocation);

        return true;
    }
}
