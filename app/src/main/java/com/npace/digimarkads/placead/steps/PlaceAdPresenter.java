package com.npace.digimarkads.placead.steps;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.npace.digimarkads.state.Thumbnail;
import com.npace.digimarkads.state.Ad;
import com.npace.digimarkads.state.AdStore;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by lubo on 3/25/2017.
 */

@Singleton
public class PlaceAdPresenter {
    public interface ThumbnailListener {
        void onDataSetChanged();

        void onErrorGeneratingThumbnail(Exception e);
    }

    private AdStore adStore;
    private Ad ad;
    private ThumbnailListener thumbnailListener;

    @Inject
    public PlaceAdPresenter(AdStore adStore) {
        this.adStore = adStore;
        this.ad = new Ad();
    }

    public void setThumbnailListener(ThumbnailListener listener) {
        this.thumbnailListener = listener;
    }

    public void addThumbnail(@NonNull Thumbnail thumbnail) {
        ad.addThumbnail(thumbnail);
        if (thumbnailListener != null) {
            thumbnailListener.onDataSetChanged();
        }
    }

    public void removeThumbnail(int index) {
        ad.removeThumbnail(index);
        if (thumbnailListener != null) {
            thumbnailListener.onDataSetChanged();
        }
    }

    public void onErrorGeneratingThumbnail(Exception e) {
        if (thumbnailListener != null) {
            thumbnailListener.onErrorGeneratingThumbnail(e);
        }
    }

    public void setMainThumbnail(int index) {
        ad.setMainThumbnail(index);
        if (thumbnailListener != null) {
            thumbnailListener.onDataSetChanged();
        }
    }

    public List<Thumbnail> getThumbnails() {
        return ad.getThumbnails();
    }

    public void setAdInfo(String title, String description, float price) {
        ad.setTitle(title);
        ad.setDescription(description);
        ad.setPrice(price);
    }

    public void setAdCategory(String category, String subcategory) {
        ad.setCategory(category);
        ad.setSubcategory(subcategory);
    }

    public void setContactInfo(@Nullable String phoneNumber, boolean shareOnFacebook, boolean showLocation) {
        ad.setPhoneNumber(phoneNumber);
        ad.setShareOnFacebook(shareOnFacebook);
        ad.setShowLocation(showLocation);
    }

    public void createAd() {
        adStore.insert(ad);
        ad = new Ad();
    }

    public int getMaxPictureCount() {
        return 6;
    }
}
