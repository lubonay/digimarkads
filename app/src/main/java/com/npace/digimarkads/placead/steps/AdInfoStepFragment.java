package com.npace.digimarkads.placead.steps;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.npace.digimarkads.DigimarkApplication;
import com.npace.digimarkads.R;

import butterknife.BindView;
import toothpick.Toothpick;

/**
 * Created by lubo on 3/21/2017.
 */

public class AdInfoStepFragment extends StepFragment implements InputValidator {
    @BindView(R.id.editTextTitle)
    EditText editTextTitle;
    @BindView(R.id.editTextDescription)
    EditText editTextDescription;
    @BindView(R.id.editTextPrice)
    EditText editTextPrice;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_ad_info, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toothpick.inject(this, Toothpick.openScope(DigimarkApplication.get()));
    }

    @Override
    public boolean validate() {
        boolean hasPrice = validateNotEmpty(editTextPrice);
        boolean hasDescription = validateNotEmpty(editTextDescription);
        boolean hasTitle = validateNotEmpty(editTextTitle);
        boolean isValid = hasTitle && hasDescription && hasPrice;

        if (isValid) {
            String title = editTextTitle.getText().toString();
            String description = editTextDescription.getText().toString();
            float price = Float.parseFloat(editTextPrice.getText().toString());
            getPresenter().setAdInfo(title, description, price);
        }

        return isValid;
    }

    private boolean validateNotEmpty(EditText editText) {
        boolean empty = editText.getText().toString().isEmpty();
        if (empty) {
            editText.requestFocus();
            editText.setError(getString(R.string.required));
        }
        return !empty;
    }
}
