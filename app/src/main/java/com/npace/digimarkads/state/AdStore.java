package com.npace.digimarkads.state;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Singleton;

/**
 * Created by lubo on 3/26/2017.
 */

@Singleton
public class AdStore {
    private List<Ad> ads;

    public AdStore() {
        ads = new ArrayList<>();
    }

    public void insert(Ad ad) {
        ads.add(ad);
        Log.i("AdStore", "Inserted " + ad.toString());
    }

    public List<Ad> getAds() {
        return Collections.unmodifiableList(ads);
    }
}
