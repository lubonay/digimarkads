package com.npace.digimarkads.state;

/**
 * Created by lubo on 3/25/2017.
 */

public class Thumbnail {
    public enum Type {
        Gallery,
        Camera;
    }

    public final Type type;
    public final String path;
    private transient boolean isMain;

    public Thumbnail(Type type, String path) {
        this.type = type;
        this.path = path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Thumbnail thumbnail = (Thumbnail) o;

        if (type != thumbnail.type) return false;
        return path != null ? path.equals(thumbnail.path) : thumbnail.path == null;

    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (path != null ? path.hashCode() : 0);
        return result;
    }

    void setMain(boolean isMain) {
        this.isMain = isMain;
    }

    public boolean isMain() {
        return isMain;
    }
}
