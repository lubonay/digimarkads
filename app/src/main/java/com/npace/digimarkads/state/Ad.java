package com.npace.digimarkads.state;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by lubo on 3/20/2017.
 */

public class Ad {
    private String title;
    private String description;
    private float price;
    private List<Thumbnail> images;
    private String category;
    private String subcategory;
    private String phone;
    private boolean should_fb_share;
    private boolean should_show_location;

    public Ad(){
        images = new ArrayList<>(6);
        phone = "";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public List<Thumbnail> getThumbnails() {
        return Collections.unmodifiableList(images);
    }

    public Thumbnail getMainThumbnail() {
        for (Thumbnail thumb : images) {
            if (thumb.isMain()) return thumb;
        }
        throw new IllegalStateException("Main thumb not found!");
    }

    public void addThumbnail(Thumbnail thumbnail) {
        if (images.isEmpty()) {
            thumbnail.setMain(true);
        }
        images.add(thumbnail);
    }

    public void removeThumbnail(int index) {
        boolean wasMain = images.get(index).isMain();
        images.remove(index);

        if (wasMain && !images.isEmpty()) {
            images.get(0).setMain(true);
        }
    }

    public void setMainThumbnail(int index) {
        if (index < 0 || index >= images.size()) {
            throw new IllegalArgumentException("Invalid thumbnail index " + index);
        }
        for (int i = 0; i < images.size(); i++) {
            Thumbnail thumbnail = images.get(i);
            thumbnail.setMain(index == i);
        }
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getPhoneNumber() {
        return phone;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phone = phoneNumber != null ? phoneNumber : "";
    }

    public boolean isShareOnFacebook() {
        return should_fb_share;
    }

    public void setShareOnFacebook(boolean shareOnFacebook) {
        this.should_fb_share = shareOnFacebook;
    }

    public boolean isShowLocation() {
        return should_show_location;
    }

    public void setShowLocation(boolean showLocation) {
        this.should_show_location = showLocation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ad ad = (Ad) o;

        if (Float.compare(ad.price, price) != 0) return false;
        if (should_fb_share != ad.should_fb_share) return false;
        if (should_show_location != ad.should_show_location) return false;
        if (title != null ? !title.equals(ad.title) : ad.title != null) return false;
        if (description != null ? !description.equals(ad.description) : ad.description != null)
            return false;
        if (images != null ? !images.equals(ad.images) : ad.images != null)
            return false;
        if (category != null ? !category.equals(ad.category) : ad.category != null) return false;
        if (subcategory != null ? !subcategory.equals(ad.subcategory) : ad.subcategory != null)
            return false;
        return phone != null ? phone.equals(ad.phone) : ad.phone == null;

    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (price != +0.0f ? Float.floatToIntBits(price) : 0);
        result = 31 * result + (images != null ? images.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (subcategory != null ? subcategory.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (should_fb_share ? 1 : 0);
        result = 31 * result + (should_show_location ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Ad{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", images=" + images +
                ", category='" + category + '\'' +
                ", subcategory='" + subcategory + '\'' +
                ", phone='" + phone + '\'' +
                ", should_fb_share=" + should_fb_share +
                ", should_show_location=" + should_show_location +
                '}';
    }
}
