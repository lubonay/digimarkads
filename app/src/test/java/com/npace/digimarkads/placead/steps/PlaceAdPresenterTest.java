package com.npace.digimarkads.placead.steps;

import com.npace.digimarkads.BuildConfig;
import com.npace.digimarkads.state.Thumbnail;
import com.npace.digimarkads.state.AdStore;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.IOException;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;


/**
 * Created by lubo on 3/25/2017.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class PlaceAdPresenterTest {
    private AdStore adStore;
    private PlaceAdPresenter presenter;
    private Thumbnail fakeThumb;
    private PlaceAdPresenter.ThumbnailListener listener;

    @Before
    public void setUp() throws Exception {
        adStore = new AdStore();
        presenter = new PlaceAdPresenter(adStore);
        fakeThumb = new Thumbnail(Thumbnail.Type.Camera, "path");
        listener = mock(PlaceAdPresenter.ThumbnailListener.class);
    }

    @Test
    public void addThumbnail_writesToState() throws Exception {
        presenter.addThumbnail(fakeThumb);

        List<Thumbnail> thumbs = presenter.getThumbnails();
        assertEquals(1, thumbs.size());
        assertEquals(fakeThumb, thumbs.get(0));
    }

    @Test
    public void addThumbnail_notifiesListener() throws Exception {
        presenter.setThumbnailListener(listener);
        presenter.addThumbnail(fakeThumb);

        verify(listener).onDataSetChanged();
        verifyNoMoreInteractions(listener);
    }

    @Test
    public void addThumbnail_doesNotNotifyForPastEvents() throws Exception {
        IOException e = new IOException("test");

        presenter.addThumbnail(fakeThumb);
        presenter.setThumbnailListener(listener);
        presenter.onErrorGeneratingThumbnail(e);

        verify(listener, never()).onDataSetChanged();
        verify(listener).onErrorGeneratingThumbnail(eq(e));
        verifyNoMoreInteractions(listener);
    }

}