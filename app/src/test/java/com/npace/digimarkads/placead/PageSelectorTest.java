package com.npace.digimarkads.placead;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * Created by lubo on 3/26/2017.
 */
public class PageSelectorTest {
    private PageSelector pageSelector;
    private PageValidator validator;

    @Before
    public void setUp() throws Exception {
        validator = mock(PageValidator.class);
    }

    @Test
    public void returnsTrue_incrementsLastPage_whenGoingForward() throws Exception {
        when(validator.validate(anyInt())).thenReturn(true);
        pageSelector = new PageSelector(0, validator);

        boolean result = pageSelector.canSelectPage(1);
        int lastPage = pageSelector.getLastSelectedPage();

        assertTrue(result);
        assertEquals(1, lastPage);
        verify(validator).validate(eq(0));
    }

    @Test
    public void returnsTrue_whenGoingBackward() throws Exception {
        pageSelector = new PageSelector(1, validator);

        boolean result = pageSelector.canSelectPage(0);
        int lastPage = pageSelector.getLastSelectedPage();

        assertTrue(result);
        assertEquals(0, lastPage);
        verifyZeroInteractions(validator);
    }

    @Test
    public void returnsFalse_doesNotIncrement_whenPageNotValid() throws Exception {
        when(validator.validate(anyInt())).thenReturn(false);
        pageSelector = new PageSelector(0, validator);

        boolean result = pageSelector.canSelectPage(1);
        int lastPage = pageSelector.getLastSelectedPage();

        assertFalse(result);
        assertEquals(0, lastPage);
        verify(validator).validate(eq(0));
    }
}