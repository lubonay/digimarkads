package com.npace.digimarkads.placead.thumb;

import com.npace.digimarkads.state.Thumbnail.Type;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static com.npace.digimarkads.placead.thumb.ThumbnailRequestCodes.*;

/**
 * Created by lubo on 3/26/2017.
 */
public class ThumbnailRequestCodesTest {
    @Test
    public void testEncodeDecode() throws Exception {
        Type camera = Type.Camera;
        Type gallery = Type.Gallery;

        int encodedCamera = encodeRequestCode(camera);
        int encodedGallery = encodeRequestCode(gallery);

        Type decodedCamera = decodeRequestCode(encodedCamera);
        Type decodedGallery = decodeRequestCode(encodedGallery);

        assertEquals(camera, decodedCamera);
        assertEquals(encodedCamera, ENCODED_CAMERA);
        assertEquals(gallery, decodedGallery);
        assertEquals(encodedGallery, ENCODED_GALLERY);
    }
}