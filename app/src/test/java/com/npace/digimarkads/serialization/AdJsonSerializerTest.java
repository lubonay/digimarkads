package com.npace.digimarkads.serialization;

import com.npace.digimarkads.state.Thumbnail;
import com.npace.digimarkads.state.Ad;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by lubo on 3/27/2017.
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class AdJsonSerializerTest {
    private AdJsonSerializer serializer;

    @Before
    public void setUp() throws Exception {
        serializer = new AdJsonSerializer();
    }

    @Test
    public void emptyAdList() throws Exception {
        JSONObject result = serializer.serialize(Collections.emptyList());

        JSONArray ads = result.getJSONArray("ads");
        assertEquals(0, ads.length());
    }

    @Test
    public void singleAd() throws Exception {
        Ad ad = new Ad();
        Thumbnail thumbnail = new Thumbnail(Thumbnail.Type.Gallery, "path_to_the_file");
        ad.addThumbnail(thumbnail);
        ad.setTitle("some title");
        ad.setCategory("For Sale");
        ad.setSubcategory("Fashion & Beauty");
        ad.setDescription("it is a thing");
        ad.setPrice(15.50f);
        ad.setPhoneNumber("+359888123123");
        ad.setShareOnFacebook(true);
        ad.setShowLocation(false);
        List<Ad> ads = Collections.singletonList(ad);

        JSONObject result = serializer.serialize(ads);
        JSONArray jsonAds = result.getJSONArray("ads");
        assertEquals(1, jsonAds.length());

        JSONObject adJson = (JSONObject) jsonAds.get(0);
        assertEquals("some title", adJson.getString("title"));
        assertEquals("For Sale", adJson.getString("category"));
        assertEquals("Fashion & Beauty", adJson.getString("subcategory"));
        assertEquals("it is a thing", adJson.getString("description"));
        assertEquals(15.50, adJson.getDouble("price"), 0.001);
        assertEquals("+359888123123", adJson.getString("phone"));
        assertEquals(true, adJson.getBoolean("should_fb_share"));
        assertEquals(false, adJson.getBoolean("should_show_location"));

        JSONArray jsonThumbs = adJson.getJSONArray("images");
        assertEquals(1, jsonThumbs.length());

        JSONObject thumbJson = (JSONObject) jsonThumbs.get(0);
        assertEquals("path_to_the_file", thumbJson.getString("path"));
        assertEquals("gallery", thumbJson.getString("type"));
    }

    @Test
    public void optionalFields() throws Exception {
        Ad ad = new Ad();

        JSONObject result = serializer.serialize(Collections.singletonList(ad));
        JSONArray jsonAds = result.getJSONArray("ads");
        assertEquals(1, jsonAds.length());

        JSONObject adJson = (JSONObject) jsonAds.get(0);
        assertEquals(0, adJson.getDouble("price"), 0.001);
        assertEquals("", adJson.getString("phone"));
        assertEquals(false, adJson.getBoolean("should_fb_share"));
        assertEquals(false, adJson.getBoolean("should_show_location"));
    }
}