package com.npace.digimarkads.state;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by lubo on 3/28/2017.
 */
public class AdTest {
    private Ad ad;
    private Thumbnail thumb1;
    private Thumbnail thumb2;

    @Before
    public void setUp() throws Exception {
        ad = new Ad();
        thumb1 = new Thumbnail(Thumbnail.Type.Gallery, "1");
        thumb2 = new Thumbnail(Thumbnail.Type.Camera, "2");
    }

    @Test
    public void setsFirstAddedThumbAsMain() throws Exception {
        thumb1.setMain(false);

        ad.addThumbnail(thumb1);

        assertTrue(thumb1.isMain());
    }

    @Test
    public void doesNotSetASecondMainThumb() throws Exception {
        ad.addThumbnail(thumb1);
        ad.addThumbnail(thumb2);

        assertFalse(thumb2.isMain());
    }

    @Test
    public void returnsMainThumbnail() throws Exception {
        ad.addThumbnail(thumb1);
        ad.addThumbnail(thumb2);

        ad.setMainThumbnail(ad.getThumbnails().indexOf(thumb2));

        assertEquals(thumb2, ad.getMainThumbnail());
    }
}